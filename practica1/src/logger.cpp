#include "stdio.h"
#include "stdlib.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <iostream>
#include <fstream>
#include "unistd.h"
using namespace std;

int main(){
    mkfifo("/tmp/mififo", 0666);
    int fp;
    if((fp = open("/tmp/mififo", O_RDONLY)) < 0){
        perror("No se ha podido abrir mififo");
        exit(-1);
    }
    cout << "FIFO abierto" << endl;
    char buf[1024];
    while(1){
        if(read(fp, buf, sizeof(buf))>0){
            //cout << "Se ha leido algo" << endl;
            cout<<buf<<endl;
        }
    }
    close(fp);
    unlink("/tmp/mififo");
    return 0;

}