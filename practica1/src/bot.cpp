#include "stdio.h"
#include "stdlib.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <iostream>
#include <fstream>
#include "unistd.h"
#include <DatosMemCompartida.h>
#include <sys/stat.h>
#include <sys/mman.h>

using namespace std;

main(){
DatosMemCompartida *direccion; int fd; struct stat atrib;
if((fd = open("file.txt", O_RDWR))<0){
    perror("Bot.cpp no ha podido abrir file.txt");
    exit(-1);
}
cout << "Se ha abierto el fichero" << endl;
fstat(fd, &atrib);
direccion = static_cast<DatosMemCompartida*>(mmap(NULL, atrib.st_size, PROT_WRITE|PROT_READ, MAP_SHARED, fd, 0));
cout << "Se ha mapeado" << endl;
close(fd);
while(1){
    usleep(25000);

    if(direccion->esfera.centro.y > ((direccion->raqueta1.y1+direccion->raqueta1.y2)/2)) direccion->accion1 = 1;
    else if(direccion->esfera.centro.y < ((direccion->raqueta1.y1+direccion->raqueta1.y2)/2)) direccion->accion1 = -1;
    else direccion->accion1 = 0;

    if(direccion->tiempo > 10){
        if(direccion->esfera.centro.y > ((direccion->raqueta2.y1+direccion->raqueta2.y2)/2)) direccion->accion2 = 1;
        else if(direccion->esfera.centro.y < ((direccion->raqueta2.y1+direccion->raqueta2.y2)/2)) direccion->accion2 = -1;
        else direccion->accion2 = 0;
    }
    //cout << "Se ha movido la raqueta1. La accion es: " << direccion->accion << endl;
}
}